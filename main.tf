terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.99.0"
    }
  }

  backend "http" {
  }
}

locals {
  grenoble_ia1_p7 = {
    principal_id = "a0c131e3-e748-461f-89a4-b344a5ed68fd"
  }

  groupe1 = {
    cferrand = {
      principal_id = "07907939-33fe-45d2-a621-347b863b8948"
    }
    mboulli = {
      principal_id = "abf6322e-4427-4cb9-b2bb-66b02369f24f"
    }
    nboukachab = {
      principal_id = "6b300ba5-e864-4cac-b380-6fc391aed854"
    }
  }

  groupe2 = {
    avasilache = {
      principal_id = "60d818b5-0a6d-4ef2-aa9c-1b7db0745d5a"
    }
    gcaferra = {
      principal_id = "beee6421-90a1-4daf-8e72-6db181182643"
    }
    wvalerio = {
      principal_id = "09842680-21ca-4310-a3bb-7c07afb58a63"
    }
  }

  groupe3 = {
    kzeghmati = {
      principal_id = "c2e0a49e-b437-4dcb-8bfd-30fc99651b76"
    }
    abroumi = {
      principal_id = "53dc4c9c-3f94-417f-a7ad-6fc4642c5b0d"
    }
    theadrapson = {
      principal_id = "ef93c8c2-6d22-4216-ac42-145b4988396f"
    }
    cbaumont = {
      principal_id = "6657a8ea-e868-496c-9180-836bc0f03cb8"
    }
  }

  groupe4 = {
    rzein = {
      principal_id = "1720026a-eef4-4cee-962a-cb4e12691471"
    }
    dardiles = {
      principal_id = "a3443234-555e-4c4b-ad6a-6f47468bf7d1"
    }
    tquach = {
      principal_id = "784d201f-dce3-4920-a541-780a356b9e68"
    }
    mdebot = {
      principal_id = "7577f92e-87a9-4479-92f3-10e030665e98"
    }
  }
}

provider "azurerm" {
  features {}
}

data "azurerm_subscription" "subscription" {
}

resource "azurerm_resource_group" "p4_rg" {
  name     = "p7-projet4"
  location = "France Central"
}

resource "azurerm_cognitive_account" "face" {
  name                = "p7-projet4-face"
  location            = azurerm_resource_group.p4_rg.location
  resource_group_name = azurerm_resource_group.p4_rg.name
  kind                = "Face"
  sku_name            = "F0"
}

resource "azurerm_role_assignment" "face_role_assignment" {
  scope                = azurerm_cognitive_account.face.id
  role_definition_name = "Contributor"
  principal_id         = local.grenoble_ia1_p7.principal_id
}

resource "azurerm_app_service_plan" "asp" {
  name                = "p7-projet4-asp"
  location            = azurerm_resource_group.p4_rg.location
  resource_group_name = azurerm_resource_group.p4_rg.name
  kind                = "Linux"
  reserved            = true

  sku {
    tier = "Basic"
    size = "B1"
  }
}

resource "azurerm_app_service" "app_service" {
  for_each = toset(["groupe1"])

  name                = "p7-projet4-app-service-${each.value}"
  location            = azurerm_resource_group.p4_rg.location
  resource_group_name = azurerm_resource_group.p4_rg.name
  app_service_plan_id = azurerm_app_service_plan.asp.id

  site_config {
    linux_fx_version          = "PYTHON|3.8"
    scm_type                  = "LocalGit"
    always_on                 = false
    use_32_bit_worker_process = true
    websockets_enabled        = true
  }
}

resource "azurerm_role_assignment" "app_service_role_assignment" {
  for_each = local.groupe1

  scope                = azurerm_app_service.app_service["groupe1"].id
  role_definition_name = "Contributor"
  principal_id         = each.value.principal_id
}
